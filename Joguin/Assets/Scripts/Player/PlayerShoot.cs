using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerShoot : MonoBehaviour
{
    RaycastHit  hit;

    void Update()
    {
        if(Input.GetButtonDown("Fire1")){
            Shoot();
        }
    }

    void Shoot(){

        float screenX = Screen.width/2;
        float screenY = Screen.height/2;

        Ray ray = Camera.main.ScreenPointToRay(new Vector3(screenX, screenY));

        if(Physics.SphereCast(ray, 0.1f, out hit))
        {
            if(hit.transform.tag == "Interactable")
            {
                Vector3 direcaoTiro = ray.direction;

                if(hit.rigidbody != null){
                    hit.rigidbody.AddForceAtPosition(direcaoTiro * 250  , hit.point);
                }

                Destroy(hit.transform.gameObject);
            }
        }
        
    }
}
