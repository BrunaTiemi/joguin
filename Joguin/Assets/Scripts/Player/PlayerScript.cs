using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerScript : MonoBehaviour
{
    public Animator anim;
    public int running;

    public bool pressedF;

    private void OnTriggerStay(Collider other) {
        switch(other.gameObject.tag){
            case "Interactable":
                if(pressedF){
                    pressedF = false;
                    print(other.gameObject.name);
                }
            break;
        }
    }
    //AnimationPar

    private void Update() {
        if(Input.GetKeyDown(KeyCode.F)){
            pressedF = true;
        }
        if(Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.D)){
            
            if(running != 1) {
                anim.SetInteger("AnimationPar", 1);
                running = 1;
            }
        }
        else if(running != 0){
            anim.SetInteger("AnimationPar", 0);
            running = 0;
        }
    }


    IEnumerator unclick(){
        yield return new WaitForSeconds(0.1f);
        pressedF = false;
    }
}
