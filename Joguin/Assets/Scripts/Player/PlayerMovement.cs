using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public CharacterController controller;

    float x, z;

    public float speed = 12f;//current move speed
    public float moveSpeed = 12f, sprintSpeed = 18f;
    public float gravity, currGravity = 9.81f, jumpHeight;

    public Transform groundCheck;
    public float groundDistance = 0.4f;
    public LayerMask groundMask;

    bool isGrounded => Physics.CheckSphere(groundCheck.position, groundDistance, groundMask);

    Vector3 velocity;
    Vector3 move;

    private void Start() {
        currGravity = gravity;
        speed = moveSpeed;
    }

    private void Update() {

        if(isGrounded && velocity.y < 0){
            velocity.y = -1f;
        }

        x = Input.GetAxis("Horizontal");
        z = Input.GetAxis("Vertical");  

        move = transform.right * x + transform.forward * z;

        Sprint();
        Jump();
        controller.Move(move * speed * Time.deltaTime);

        //currGravity
        velocity.y -= currGravity *Time.deltaTime;
        controller.Move(velocity * Time.deltaTime);
    }

    void Sprint(){
        if(Input.GetKeyDown(KeyCode.LeftShift)){
            speed = sprintSpeed;
        }
        else{
            speed = moveSpeed;
        }
    }

    void Jump(){
        if(Input.GetKeyDown(KeyCode.Space) && isGrounded){
            velocity.y += jumpHeight;
        }
    }

    private void OnTriggerEnter(Collider other) {
        switch(other.gameObject.tag){
            case "ZeroGravity": ZeroGravity(true);
            break;
        }
    }

    private void OnTriggerExit(Collider other) {
        switch(other.gameObject.tag){
            case "ZeroGravity": ZeroGravity(false);
            break;
        }
    }

    void ZeroGravity(bool state){
        if(state){
            currGravity = 0;
        }
        else currGravity = gravity;
    }
}
